<s>Twine</s> Nylon
=====

Introduction
------------
Nylon is built on top of the old Twine 1 engine, which is a horrific bitch to work with.
Twine 1 is no longer in development, but Nylon is.  Nylon extends Twine by rebuilding the
compile process to be more accessible and flexible programmatically, as well as providing
a far more robust AST for error checking, code transformation and analysis.

A web-application "sequel" to Twine 1, called Twine 2, is in active development.
Its repositories are [here](https://bitbucket.com/klembot/twinejs) and [here](https://bitbucket.com/_L_/harlowe).

This repository is here to retain the GPL3 components of Twine, such as the JS and themes, and prevent pollution of
Nylon itself, which is MIT.  This software will no longer run by itself.

Refer to the Nylon repository for Nylon build instructions.
